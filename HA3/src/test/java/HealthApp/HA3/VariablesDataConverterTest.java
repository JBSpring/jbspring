package HealthApp.HA3;



import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;
import measurement.VariablesDataConverter;

public class VariablesDataConverterTest {
	private VariablesDataConverter vdc;

	@Before
	public void setUp() throws Exception {
		vdc = new VariablesDataConverter();
	}

	@Test
	public void testKgToPounds() {
		
		// When
		double result = vdc.kgToPounds(10);
		
		// Then
		Assert.assertEquals(22.046226218, result, 0.05);
	}

	@Test
	public void testPoundsToKg() {
		double result = vdc.poundsToKg(220);
	//	Assert.assertEquals(99.79032140000001, result);
		
		Assert.assertEquals(99.7903214, result, 0.05);  
	}

	@Test
	public void testCmToInches() {
		double result = vdc.cmToInches(10);
		Assert.assertEquals(3.93700787, result, 0.05);
	}
	
	@Test
	public void testInchesToM() {
		double result = vdc.inchesToM(10);
		Assert.assertEquals(0.254, result, 0.05);
	}
	
}
