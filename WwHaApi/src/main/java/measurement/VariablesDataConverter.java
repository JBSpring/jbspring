package measurement;

/*
 * 1 kg = 2,20462 font (lbs); 1 font = 0,453592 kg.
 */
public class VariablesDataConverter {
	public double weight;
	public double height;
	public double lb; // 1 pound = 0.45359237 kilograms
	public double inches; // 1 inch = 2.54 cm

	public VariablesDataConverter() {

	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getLb() {
		return lb;
	}

	public void setLb(double lb) {
		this.lb = lb;
	}

	public double getInches() {
		return inches;
	}

	public void setInches(double inches) {
		this.inches = inches;
	}

	public VariablesDataConverter(double inches, double lb) {
		this.inches = inches;
		this.lb = lb;
	}
	//TODO complex converter
	public double kgToPounds(double weight) {
		return weight * 2.2046226218;
	}

	public double poundsToKg(double pounds) {
		return pounds * 0.45359237;
	}

	public double cmToInches(double height) {
		return height * 0.393700787;
	}

	public double inchesToM(double inches) {
		return inches * 0.0254;
	}
	
	@Override
	public String toString() {
		return null;
	}
}