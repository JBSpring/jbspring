package HealthApp.HA3;

import measurement.Compositions;

import measurement.VariablesDataConverter;
import measurement.converterBMI;

/**
 * @author Jozsef Fulop 
 *
 */
public class App {
	public static void main(String[] args) {

		converterBMI cbmi = new converterBMI();

		VariablesDataConverter vc = new VariablesDataConverter();

		double i = vc.inchesToM(68);
		double p = vc.poundsToKg(220);
		
		double result = cbmi.convertBMI(i, p);
		System.out.println("bmi: " + result);

		Compositions c = new Compositions();
		System.out.println(c.Composition(result));
		
		double result2 = cbmi.convertBMI(1.82, 70);
		System.out.println("bmi: " + result2);
		
		System.out.println(c.Composition(result2));
		
	}
}
