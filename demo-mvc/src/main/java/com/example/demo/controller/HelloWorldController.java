package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.repository.VocableRepository;
import com.example.service.VocableService;

@Controller
public class HelloWorldController {
	
	private VocableService vocableService;

	@Autowired
	public void setVocableService(VocableService vocableService) {
		this.vocableService = vocableService;
	}
//	
//	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "text/plain")
//	public String helloWorld(Model model) {
//		model.addAttribute("message", "HelloWorld!!!!!!!!");
//		model.addAttribute("pageTitle", "oldalcím");
//
//		return "voca";
//	}

		
	@RequestMapping("/")
	public String voca(Model model) {
		model.addAttribute("vocable",  vocableService.getVocable());
		//System.out.println("valami" + vocableService.getVocable());
		return "voca";
	}
	
	
}
