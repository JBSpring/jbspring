package com.example.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
// @Table(name = "vocable")
public class Vocable {

	@GeneratedValue
	@Id
	private Long id;

	private String word;
	private String meaning;

	private Vocable() {

	}

	public Vocable(Long id, String word, String meaning) {

		this.id = id;
		this.word = word;
		this.meaning = meaning;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public String getMeaning() {
		return meaning;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

	@Override
	public String toString() {
		return "Vocable [id=" + id + ", word=" + word + ", meaning=" + meaning + "]";
	}

}
